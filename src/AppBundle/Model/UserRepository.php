<?php

namespace AppBundle\Model;

use Symfony\Component\DependencyInjection\Container;

/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 31/12/14
 * Time: 12:46
 */
class UserRepository
{
	/** @var SplObjectStorage|User[] */
	protected $users;

	/**
	 * @var Container
	 *
	 */
	private $container;

	/**
	 * Create UserRepository
	 * @param Container $container
	 */
	public function __construct(Container $container)
	{

		$this->container = $container;
		$this->users = new \SplObjectStorage();

		$this->users->attach(new User(1034, 'Rafael', 'rafael@example.com'));
		$this->users->attach(new User(1035, 'Donatello', 'donatello@example.com'));
		$this->users->attach(new User(1036, 'Michelangelo', 'michelangelo@example.com'));
		$this->users->attach(new User(1037, 'Leonardo', 'leonardo@example.com'));
	}

	/**
	 * Get all users
	 * @return SplObjectStorage|User[]|\SplObjectStorage
	 */
	public function getAll(){
		return $this->users;
	}


	/**
	 * Get user by user_id
	 * @param $user_id
	 * @return User|mixed|null|object
	 */
	public function getUserById($user_id){
		foreach ($this->getAll() as $user) {
			if($user->getId() == $user_id){
				return $user;
			}
		}
		return null;
	}


	/**
	 * Simulate save user object.
	 * In production I'll use a User as Entity, and Subscribber to handle this event.
	 * Example of Subscibber attached in Service
	 * @param User $user
	 */
	public function save(User $user){



		if($user->isEmailChanged()) {
			Log::createByObject($user);
			//user save
			$this->container->get("stats_system")->postRequest($user);
			$this->container->get("marketing_system")->postRequest($user);
		}


	}




}