<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 09/01/15
 * Time: 17:13
 */

namespace AppBundle\Model;

/**
 * Class Log - abstract create log object
 * @package AppBundle\Model
 */
class Log {
    /**
     * @var
     */
    protected $objectName, $objectId, $date, $data;


    /**
     * @param $objectId
     * @return $this
     */
    public function setObjectId($objectId){
        $this->objectId =$objectId;
        return $this;
    }

    /**
     * @param $objectName
     * @return $this
     */
    public function setObjectName($objectName){
        $this->setObjectName =$objectName;
        return $this;
    }

    /**
     * @param $date
     * @return $this
     */
    public function setDate($date){
        $this->date =$date;
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data){
        $this->data =$data;
        return $this;
    }


    /**
     * Create log object based on other object
     * @param LogInterface $object
     * @return Log
     */
    static function createByObject(LogInterface $object){
        $log = new Log();

        $log->setObjectId($object->getId());
        $log->setObjectName(get_class($object));
        $log->setDate(new \DateTime("now"));
        $log->setData($object->toArray());

        return $log;

    }
}