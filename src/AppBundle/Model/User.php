<?php

namespace AppBundle\Model;

/**
 * Class User
 * @package AppBundle\Model
 */
class User implements LogInterface
{
	protected $id, $name, $email;


	private $emailChanged = false;
	/**
	 * @param $id
	 * @param $name
	 * @param $email
	 */
	function __construct($id, $name, $email)
	{
		$this->id    = $id;
		$this->name  = $name;
		$this->email = $email;
	}


	/**
	 * @return integer
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(){
		return $this->name;

	}

	/**
	 * @return string
	 */
	public function getEmail(){
		return $this->email;
	}


	/**
	 * @param $email
	 * @return $this
	 */
	public function setEmail($email){

		if($email != $this->getEmail()){
			$this->emailChanged = true;
		}
		$this->email = $email;
		return $this;
	}

	/**
	 * @return array
	 */
	public function toArray(){
		return [
			'id'=>$this->getId(),
			'name'=>$this->getName(),
			'email'=>$this->getEmail()
		];
	}

	/**
	 * Return true if email was modified
	 */
	public function isEmailChanged(){
		return $this->emailChanged;
	}




}