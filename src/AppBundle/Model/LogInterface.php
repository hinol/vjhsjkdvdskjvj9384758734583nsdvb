<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 09/01/15
 * Time: 17:18
 */

namespace AppBundle\Model;

/**
 * Interface LogInterface - requirements for object to create logs
 * @package AppBundle\Model
 */
interface LogInterface {

    /**
     * Get object ID
     * @return mixed
     */
    public function getId();


    /**
     * Create array from object
     * @return mixed
     */
    public function toArray();


}