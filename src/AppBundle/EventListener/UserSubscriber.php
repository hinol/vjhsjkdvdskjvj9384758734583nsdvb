<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 08/10/14
 * Time: 14:56
 */

namespace AppBundle\EventListener;

use AppBundle\Model\Log;
use AppBundle\Model\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UserSubscriber
 * @package AppBundle\EventListener
 */
class UserSubscriber implements EventSubscriber
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {


        return array(
//            'prePersist',
//            'postPersist',
            'postUpdate',
//            'preUpdate',
        );
    }

    /**
     * @param ContainerInterface $container
     * @return $this
     */
    public function setContainer(ContainerInterface $container)
    {

        $this->container = $container;
        return $this;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        // pre update event

    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {



    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $em = $args->getEntityManager();


        if ($entity instanceof User) {

            if($entity->isEmailChanged()){

                //create log
                Log::createByObject($entity);
                //user save
                $this->container->get("stats_system")->postRequest($entity);
                $this->container->get("marketing_system")->postRequest($entity);

            }


        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {

    }


}

