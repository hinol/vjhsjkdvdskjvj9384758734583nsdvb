<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Model\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Route("/index", name="user_index")
     * @Template()
     */
    public function indexAction()
    {
        $users = $this->get("user_repository")->getAll();

        return [
          'users'=>$users
        ];
    }

    /**
     * @Route("/edit/{user_id}", name="user_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Request $request, $user_id)
    {

        $user = $this->get("user_repository")->getUserById($user_id);

        if(!($user instanceof User)){
            throw new \Exception("Bad user id parameter");
        }
        $form = $this->createEditForm($user);

        return [
            'form'=>$form->createView()
        ];
    }

    /**
     * @Route("/update/{user_id}", name="user_update")
     * @Method("POST")
     * @Template("@App/User/edit.html.twig")
     */
    public function updateAction(Request $request, $user_id)
    {
        $user = $this->get("user_repository")->getUserById($user_id);

        if(!($user instanceof User)){
            throw new \Exception("Bad user id parameter");
        }

        $form = $this->createEditForm($user);
        $form->handleRequest($request);
        if($form->isValid()){

            $this->get("user_repository")->save($user);

            $request->getSession()->getFlashBag()->add(
                'notice',
                'Your changes were saved!'
            );

            return $this->redirectToRoute("user_edit", ['user_id'=>$user->getId()]);
        }

        return [
            'form'=>$form->createView()
        ];
    }


    /**
     * @param User $user
     * @return \Symfony\Component\Form\Form
     * Create user email edit form
     */

    private function createEditForm(User $user){
        return $this->createForm(new UserType(), $user, [
            'action'=>$this->generateUrl("user_update", ['user_id'=>$user->getId()]),
            'method'=>'post'
        ]);
    }

}
