<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Model\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class UserApiController
 * @package AppBundle\Controller
 * @Route("/api")
 */
class UserApiController extends Controller
{

    /**
     * @Route("/list")
     * @Method("GET")
     * @return JsonResponse
     *
     */
    public function listAction(){
        $users = $this->get("user_repository")->getAll();
        $result = [];
        foreach ($users as $user) {
            $result[] = $user->toArray();
        }
        return new JsonResponse(['data'=>$result]);

    }


    /**
     * @param Request $request
     * @param $user_id
     * @return JsonResponse
     * @Route("/editEmail/{user_id}")
     * @Method("POST")
     */
    public function editEmailAction(Request $request, $user_id){
        $user = $this->get("user_repository")->getUserById($user_id);
        if(!($user instanceof User)){
            return new JsonResponse(['status'=>'error', 'message'=>'Bad user id']);
        }

        $email = $request->get("email");

        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            $user->setEmail($email);
            $this->get("user_repository")->save($user);
            return new JsonResponse(['status'=>'ok', 'message'=>'User email changed', 'data'=>$user->toArray()]);
        }else{
            return new JsonResponse(['status'=>'error', 'message'=>'Bad post data']);
        }
    }
}
