<?php
/**
 * Created by PhpStorm.
 * User: mzeromski
 * Date: 09/01/15
 * Time: 17:22
 */

namespace AppBundle\Command;


use AppBundle\Model\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UserCommand extends ContainerAwareCommand
{

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('app:user')
            ->setDescription('Command for user operations')
            ->addArgument('operation', InputArgument::REQUIRED, 'Who do you want to do? (list|edit)')
            ->addOption('user_id', null, InputOption::VALUE_OPTIONAL, 'User Id to change')
            ->addOption('user_email', null, InputOption::VALUE_OPTIONAL, "new user email");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $operation = $input->getArgument('operation');
        $container = $this->getContainer();
        switch ($operation) {
            case "list":
                foreach ($container->get("user_repository")->getAll() as $user) {
                    $output->writeln(json_encode($user->toArray()));
                }
                break;
            case "edit":

                $user = $container->get("user_repository")->getUserById($input->getOption('user_id'));
                if ($user instanceof User) {

                    $output->writeln("Edit user: ".$user->getId());
                    if($email = $input->getOption("user_email")){
                        $user->setEmail($email);
                        $container->get("user_repository")->save($user);
                        $output->writeln("User email changed to : ".$email);
                    }

                } else {
                    throw new \Exception("Bad user_id option");
                }


                break;
        }
    }


}