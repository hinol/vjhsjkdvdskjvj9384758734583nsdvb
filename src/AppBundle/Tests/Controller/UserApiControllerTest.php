<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserApiControllerTest extends WebTestCase
{

    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/api/list');
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
        $this->assertTrue($client->getResponse()->isSuccessful());
    }



    public function testEdit()
    {
        $client = static::createClient();



        $crawler = $client->request('POST', '/api/editEmail/1034', ['email'=>'test@wp.pl']);


        $content = $client->getResponse()->getContent();

        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
        
        $this->assertTrue((bool)preg_match('/changed/', $content));
    }
}
