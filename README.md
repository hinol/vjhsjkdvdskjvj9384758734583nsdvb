Implement user email change use case.

Use case should be used in 3 places:
- on web page, using symfony form
- on cli using symfony command
- on api endpoint using http request

Additional requirements:
- notify external stats system about user email change (service stats_system)
- notify external marketing system about user email change (service marketing_system)
- change log

You can use existing existins User and UserRepository classes in AppBundle\Model namespace



Solution description

1. User edit using SF2 Form: /index

2. By command
 a)  ./app/console  app:user list < user list
 b)  ./app/console  app:user edit --user_id=1034 --user_email=asc@wp.pl

3. By API
 a) Everything is implemented in: UserApiController
 a) UserApiControllerTest < api edit test


